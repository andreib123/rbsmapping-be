package com.ericsson.interactivemapservice.pojo;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

@Deprecated
// deprecated POJO to be returned initially, containing RbsLocation objects
public class MapRbsStore {
	
	@JsonProperty
    @JacksonXmlElementWrapper(localName = "rbss")
    @JacksonXmlProperty(localName = "rbs")
	List<RbsLocation> rbss;
	
	public MapRbsStore(final List<RbsLocation> list) {
		this.rbss = list;
	}
	
	@JsonIgnore
	public List<RbsLocation> getRbss() {
		return rbss;
	}

}
