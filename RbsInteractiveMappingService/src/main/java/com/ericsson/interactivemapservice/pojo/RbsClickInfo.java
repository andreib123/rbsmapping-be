package com.ericsson.interactivemapservice.pojo;

import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

// CURRENT RBS CLICK INFO (deprecated as returning arraylist only)
@Deprecated
public class RbsClickInfo {
	@JsonProperty("cell")
    @JacksonXmlElementWrapper(localName = "cells")
	ArrayList<CellClickEntity> cells;
	
	public RbsClickInfo(final ArrayList<CellClickEntity> list) {
		this.cells = list;
	}
	
	@JsonIgnore
	public ArrayList<CellClickEntity> getCells() {
		return cells;
	}
}