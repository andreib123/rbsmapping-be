package com.ericsson.interactivemapservice.pojo;

@Deprecated
public class RbsForSerialization {
	private long numCells;
	private float latitude;
	private float longitude;
	private String eNodeBID;
	
	public RbsForSerialization(long numCells, float latitude, float longitude, String eNodeBID) {
		this.numCells = numCells;
		this.latitude = (float) (latitude / 10000000.0);
		this.longitude = (float) (longitude / 10000000.0);
		this.eNodeBID = eNodeBID;
	}

	public long getNumCells() {
		return numCells;
	}

	public float getLatitude() {
		return latitude;
	}

	public float getLongitude() {
		return longitude;
	}

	public String geteNodeBID() {
		return eNodeBID;
	}
	
}
