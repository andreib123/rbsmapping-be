package com.ericsson.interactivemapservice.pojo;

import java.util.ArrayList;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;

@Deprecated
// deprecated Cell POJO
public class Cell {
	
	private final String operator;
	private final long operatorCellId;
	private final int azimuth;
	private ArrayList<String> services = new ArrayList<String>();
	
	public Cell(String operator, long operatorCellId, int azimuth, ArrayList<String> services) {
		this.operator = operator;
		this.operatorCellId = operatorCellId;
		this.azimuth = azimuth;
		this.services = services;
	}

	public String getOperator() {
		return operator;
	}

	public long getOperatorCellId() {
		return operatorCellId;
	}

	public int getAzimuth() {
		return azimuth;
	}

	@JacksonXmlElementWrapper(localName = "services")
	public ArrayList<String> getValue() {
		return services;
	}
	
}
