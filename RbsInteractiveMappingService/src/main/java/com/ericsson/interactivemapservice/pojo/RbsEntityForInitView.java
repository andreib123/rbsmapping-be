package com.ericsson.interactivemapservice.pojo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Immutable;

import com.fasterxml.jackson.annotation.JsonIgnore;

// CURRENT WORKING ENTITY FOR RBS INIT VIEW
@Entity
@Immutable
@Table(name = "sectors")
public class RbsEntityForInitView {
	@Id
	@Column(name = "geohash")
	private String geohash;
	
	@Column(name = "cellType")
	private String cellType;
	
	@Column(name = "eNodeBID")
	private String eNodeBID;
	
	@Column(name = "region")
	private String region;
	
	@Column(name = "tac")
	private int tac;
	
	@Column(name = "RETCapable")
	private int RETCapable;
	
	@Column( name = "freqBand")
	private int freqBand;

	public RbsEntityForInitView() {}

	public RbsEntityForInitView(String geohash, String cellType, String eNodeBID, String region, int tac, int RETCapable, int freqBand) {
		this.geohash = geohash;
		this.cellType = cellType;
		this.eNodeBID = eNodeBID;
		this.region = region;
		this.tac = tac;
		this.RETCapable = RETCapable;
		this.freqBand = freqBand;

	}

	public String getGeohash() {
		return geohash;
	}

	public String getCellType() {
		return cellType;
	}
	
	public String getENodeBID() {
		return eNodeBID;
	}
	
	public String getRegion() {
		return region;
	}
	
	public int getTac() {
		return tac;
	}
	
	public int getRETCapable() {
		return RETCapable;
	}
	
	public int getFreqBand() {
		return freqBand;
	}
	


}
