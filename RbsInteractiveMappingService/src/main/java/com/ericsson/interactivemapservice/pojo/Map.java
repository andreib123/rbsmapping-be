package com.ericsson.interactivemapservice.pojo;

import java.util.ArrayList;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

@Deprecated
// deprecated
@JacksonXmlRootElement(localName = "map")
public class Map {
	
	//@JacksonXmlElementWrapper(useWrapping = false)
	private ArrayList<Rbs> rbs;
	
	public Map(ArrayList<Rbs> rbs) {
		this.rbs = rbs;
	}
	
	@JacksonXmlElementWrapper(useWrapping = false)
	public ArrayList<Rbs> getRbs() {
		return rbs;
	}

}
