package com.ericsson.interactivemapservice.pojo;

import java.util.ArrayList;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

// CURRENT SERIALIZED CLASS FOR AN RBS
public class RbsInit {
	private long numCells;
	private String eNodeBID;
	private double latitude;
	private double longitude;
	private String region;
	private int tac;
	private int RETCapable;
	private int freqBand;


    @JacksonXmlElementWrapper(localName = "geohashes")
    @JsonProperty("geohashes")
	private ArrayList<String> geohashes;

	public RbsInit(long numCells, String eNodeBID, double latitude, double longitude, String region, int tac, int RETCapable, int freqBand, Set<String> geohashSet) {
		this.numCells = numCells;
		this.eNodeBID = eNodeBID;
		this.latitude = latitude;
		this.longitude = longitude;
		this.region = region;
		this.tac = tac;
		this.RETCapable = RETCapable;
		this.freqBand = freqBand;
		this.geohashes = new ArrayList<>(geohashSet);
	}

	public long getNumCells() {
		return numCells;
	}

	public String geteNodeBID() {
		return eNodeBID;
	}

	public double getLatitude() {
		return latitude;
	}

	public double getLongitude() {
		return longitude;
	}
	
	public String getRegion() {
		return region;
	}
	
	public int getTac() {
		return tac;
	}
	
	public int getRETCapable() {
		return RETCapable;
	}
	
	public int getFreqBand() {
		return freqBand;
	}
	
	
	public ArrayList<String> getGeohashes() {
		return geohashes;
	}

	public void setNumCells(long numCells) {
		this.numCells = numCells;
	}

	public void seteNodeBID(String eNodeBID) {
		this.eNodeBID = eNodeBID;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	
	public void setRegion(String region) {
		this.region = region;
	}
	
	public void setTac(int tac) {
		this.tac = tac;
	}
	
	public void setRETCapable(int RETCapable) {
		this.RETCapable = RETCapable;
	}
	
	public void setFreqband(int freqBand) {
		this.freqBand = freqBand;
	}
	
	
	public void setGeohashes(ArrayList<String> geohashes) {
		this.geohashes = geohashes;
	}

}
