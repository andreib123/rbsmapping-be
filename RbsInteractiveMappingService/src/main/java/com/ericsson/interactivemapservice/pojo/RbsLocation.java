package com.ericsson.interactivemapservice.pojo;

@Deprecated
// RBS POJO to be returned
public class RbsLocation {
	private String eNodeBID;
	private double latitude;
	private double longitude;
	
	public RbsLocation(String eNodeBID, double latitude, double longitude) {
		this.eNodeBID = eNodeBID;
		this.latitude = latitude;
		this.longitude = longitude;
	}
	
	public String geteNodeBID() {
		return eNodeBID;
	}
	public double getLatitude() {
		return latitude/10000000;
	}
	public double getLongitude() {
		return longitude/10000000;
	}
	
	
}
