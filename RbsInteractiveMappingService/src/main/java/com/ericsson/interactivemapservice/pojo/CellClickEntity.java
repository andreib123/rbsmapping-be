package com.ericsson.interactivemapservice.pojo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.lang.Math;

import org.elasticsearch.geo.utils.Geohash;
import org.hibernate.annotations.Immutable;

import com.fasterxml.jackson.annotation.JsonInclude;

// CURRENT CELL ENTITY
@Entity
@Immutable
@Table(name = "sectors")
public class CellClickEntity {
	
	@Column(name = "eNodeBID")
	private String eNodeBID;
	
	@Column(name = "geohash")
	private String geohash;
	
	@Column(name = "market")
	private String market;
	
	@Column(name = "region")
	private String region;
	
	@Column(name = "cellType")
	private String cellType;
	
	// unique ID for each instance
	@Id
	@Column(name = "cgi")
	private String cgi;
	
	@Column(name = "azimuth")
	private String azimuth;
	
	@Column(name = "freqBand")
	private double freqBand;

	@Column(name = "tac")
	private int tac;

	@Column(name = "RETCapable")
	private int RETCapable;
	
	public CellClickEntity() {}
	
	public CellClickEntity(String eNodeBID, String geohash, String market, String region, String cellType, String cgi, String azimuth, double freqBand, int tac, int RETCapable) {
		this.eNodeBID = eNodeBID;
		this.geohash = geohash;
		this.market = market;
		this.region = region;
		this.cellType = cellType;
		this.cgi = cgi;
		this.azimuth = azimuth;
		this.freqBand = freqBand;
		this.tac = tac;
		this.RETCapable = RETCapable;
	}
	
	public String getENodeBID() {
		return eNodeBID;
	}

	public String getGeohash() {
		return geohash;
	}

	public String getMarket() {
		return market;
	}

	public String getRegion() {
		return region;
	}

	public String getCellType() {
		return cellType;
	}
	
	public String getCgi() {
		return cgi;
	}
	
	public String getAzimuth() {
		return azimuth;
	}
	
	public double getFreqBand() {
		return freqBand;
	}

	public int getTac() { return tac; }

	public int getRETCapable() { return RETCapable; }
	
	// for serialization of a "latitude" element
	public double getLatitude() {
		return Math.round(Geohash.decodeLatitude(geohash) * 10000.0) / 10000.0;
	}
	
	// for serialization of a "longitude" element
	public double getLongitude() {
		return Math.round(Geohash.decodeLongitude(geohash) * 10000.0) / 10000.0;
	}

}
