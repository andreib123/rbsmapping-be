package com.ericsson.interactivemapservice.pojo.testing;


public class Test{
	   public static void main(String args[]) {

	   		UseFoo useFoo = new UseFoo();

	   		Foo foo = new Foo() {
	   			@Override
				public String method(String string) {
	   				return string + " from Foo inner class definition";
				}
			};
	   		Foo foo2 = param -> param + " from Foo lambda definition";

	   		String result = useFoo.add("Message", foo2);
	   		System.out.println(result);



	   }
	}
