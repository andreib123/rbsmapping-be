package com.ericsson.interactivemapservice.pojo;

import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

@Deprecated
// POJO that contains list of cells for an RBS
public class RbsCellStore {
	
	@JsonProperty
    @JacksonXmlElementWrapper(localName = "cells")
    @JacksonXmlProperty(localName = "cell")
	ArrayList<CellEntity> cells;
	
	public RbsCellStore(ArrayList<CellEntity> cells) {
		this.cells = cells;
	}
	
	public ArrayList<CellEntity> getCells() {
		return cells;
	}

}
