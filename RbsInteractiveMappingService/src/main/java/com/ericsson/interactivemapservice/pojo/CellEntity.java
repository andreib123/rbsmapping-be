package com.ericsson.interactivemapservice.pojo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Immutable;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

@Deprecated
// deprecated Cell POJO
@Entity
@Immutable
@Table(name = "sectors")
public class CellEntity   {

	@Id
	@Column(name = "cellId")
	private String cellId;

	@Column(name = "azimuth")
	private String azimuth;

	@Column(name = "cellType")
	private String cellType;

	@Column(name = "eNodeBID")
	private String eNodeBID;

	@Column(name= "latitude")
	private float latitude;

	@Column(name= "longitude")
	private float longitude;
	
	public CellEntity(String eNodeBID, String azimuth, String cellId, String cellType, float latitude, float longitude) {
		this.eNodeBID = eNodeBID;
		this.azimuth = azimuth;
		this.cellId = cellId;
		this.cellType = cellType;
		this.latitude = latitude;
		this.longitude = longitude;
	}

	public CellEntity() {}
	
	public String getENodeBID() {
		return eNodeBID;
	}

	public String getAzimuth() {
		return azimuth;
	}
	
	public String getCellId() {
		return cellId;
	}
	
	public String getCellType() {
		return cellType;
	}
	
	@JsonIgnore()
	public String geteNodeBID() {
		return eNodeBID;
	}

	public float getLatitude() {
		return latitude/10000000;
	}

	public float getLongitude() {
		return longitude/10000000;
	}

}
