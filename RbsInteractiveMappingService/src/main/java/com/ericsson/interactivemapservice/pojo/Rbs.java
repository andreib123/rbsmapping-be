package com.ericsson.interactivemapservice.pojo;

import java.util.ArrayList;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;

@Deprecated
// deprecated RBS POJO
public class Rbs {
	
	private final String area;
	private final long siteId;
	private Location location;
	private ArrayList<Cell> cell;

	public Rbs(String area, long siteId, Location location, ArrayList<Cell> cell2) {
		this.area = area;
		this.siteId = siteId;
		this.location = location;
		this.cell = cell2;
	}

	public String getArea() {
		return area;
	}

	public long getSiteId() {
		return siteId;
	}

	public Location getLocation() {
		return location;
	}

	@JacksonXmlElementWrapper(localName = "cells") //the "cells" element wrapper around "cell" elements
	public ArrayList<Cell> getCell() { // "getCell" with no s at the end specifies that each element in the list is a "cell" and not a "cells"
		return cell;
	}
	
	
}
