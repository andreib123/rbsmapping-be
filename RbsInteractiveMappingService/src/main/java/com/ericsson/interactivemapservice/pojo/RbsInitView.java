package com.ericsson.interactivemapservice.pojo;

import org.elasticsearch.geo.utils.Geohash;

// CURRENT: Objects of this class are created when querying the DB using RbsEntityForInitView 
public class RbsInitView {
	// number of cells at location
	private long numCells;
	
	// 12 symbol geohash value for location
	private String geohash;

	// eNodeBID of location defined by geohash
	private String eNodeBID;
	
	// rounded to 4 decimal places
	private double latitude;
	private double longitude;
	
	private String region;
	private int tac;
	private int RETCapable;
	private int freqBand;

	
	public RbsInitView(long numCells, String geohash, String eNodeBID, String region, int tac, int RETCapable, int freqBand) {
		this.numCells = numCells;
		this.geohash = geohash;
		this.eNodeBID = eNodeBID;
		this.latitude = Geohash.decodeLatitude(geohash);
		this.longitude = Geohash.decodeLongitude(geohash);
		this.region = region;
		this.tac = tac;
		this.RETCapable = RETCapable;
		this.freqBand = freqBand;

		
	}
	
	public long getNumCells() {
		return numCells;
	}
	
	public double getLatitude() {
		return latitude;
	}

	public double getLongitude() {
		return longitude;
	}
	
	public String getGeohash() {
		return geohash;
	}
	
	public String getENodeBID() {
		return eNodeBID;
	}
	
	public String getRegion() {
		return region;
	}
	
	public int getTac() {
		return tac;
	}
	
	public int getRETCapable() {
		return RETCapable;
	}
	
	public int getFreqBand() {
		return freqBand;
	}
	

	
	
	@Override
	public boolean equals(Object o) {
		
		if(this == o) return true;
		
		if((o == null) || (getClass() != o.getClass())) return false;
		
		RbsInitView r = (RbsInitView) o;
		
		if((latitude == r.getLatitude()) && (longitude == r.getLongitude())) return true;
		
		int distanceBetweenCoords = distBetweenCoordsInM(r);
		
		return (distanceBetweenCoords < 500);
	}

	@Override
	public int hashCode() {
		// use latitude and longitude instance variables only for consistency with equals method
		int prime = 31;
		int result = 1;
		result = prime * result + (int) latitude;
		result = prime * result + (int) longitude;
		return result;
	}
	
	// return the distance between two coordinates in metres
	private int distBetweenCoordsInM(RbsInitView r) {
		// radius of the earth in km
		final double earthRadius = 6371.0;
		
		double diffRadLat = Math.toRadians(latitude - r.getLatitude());
		double diffRadLon = Math.toRadians(longitude - r.getLongitude());
		
		double radLat1 = Math.toRadians(latitude);
		double radLat2 = Math.toRadians(r.getLatitude());

		// sin()^2 + 
		double a = Math.pow(Math.sin(diffRadLat/2.0), 2.0) + Math.pow(Math.sin(diffRadLon/2.0), 2.0) * Math.cos(radLat1) * Math.cos(radLat2);
		
		double c = 2.0 * Math.atan2(Math.sqrt(a), Math.sqrt(1.0-a));
		
		Double dist = (earthRadius * c) * 1000;
		
		return dist.intValue();
	}

	@Override
    public String toString() {
	    return "numCells: "    + numCells  +
                " geohash: "   + geohash   +
                " eNodeBID: "  + eNodeBID  +
                " latitude: "  + latitude  +
                " longitude: " + longitude +
                " region: " + region +
                " tac: " + tac +
                " RETCapable: " + RETCapable +
                " freqBand: " + freqBand +
                "\n";
    }

}
