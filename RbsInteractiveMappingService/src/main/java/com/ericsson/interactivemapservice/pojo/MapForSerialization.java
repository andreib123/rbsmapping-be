package com.ericsson.interactivemapservice.pojo;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

//DEPRECATED SERIALIZED WRAPPER FOR RBS INIT VIEW
@Deprecated
public class MapForSerialization {

	@JsonProperty
	@JacksonXmlElementWrapper(localName = "rbss")
	@JacksonXmlProperty(localName = "rbs")
	ArrayList<RbsForSerialization> newRbsList;
	
	public MapForSerialization(final ArrayList<RbsForSerialization> list) {
		this.newRbsList = list;
	}
	
	@JsonIgnore
	public List<RbsForSerialization> getRbss() {
		return newRbsList;
	}

}