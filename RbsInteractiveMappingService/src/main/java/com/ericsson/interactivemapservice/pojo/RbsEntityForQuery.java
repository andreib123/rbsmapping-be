package com.ericsson.interactivemapservice.pojo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Immutable;

@Deprecated
@Entity
@Immutable
@Table(name = "sectors")
public class RbsEntityForQuery {
	@Id
	@Column(name = "cgi")
	private String cgi;

	@Column(name = "eNodeBID")
	private String eNodeBID;
	
	@Column(name = "latitude")
	private float latitude;
	
	@Column(name = "longitude")
	private float longitude;
	
	@Column(name = "cellType")
	private String cellType;
	
	public RbsEntityForQuery() {}

	public RbsEntityForQuery(String cgi, String eNodeBID, float latitude, float longitude) {
		this.cgi = cgi;
		this.eNodeBID = eNodeBID;
		this.latitude = latitude;
		this.longitude = longitude;
	}
	
	public String getCgi() {
		return cgi;
	}

	public String geteNodeBID() {
		return eNodeBID;
	}

	public float getLatitude() {
		return latitude;
	}

	public float getLongitude() {
		return longitude;
	}

}
