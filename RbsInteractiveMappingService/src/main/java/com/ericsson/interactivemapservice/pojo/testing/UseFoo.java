package com.ericsson.interactivemapservice.pojo.testing;

public class UseFoo {

    public UseFoo() {}

    public String add(String string, Foo foo) {
        return foo.method(string);
    }
}
