package com.ericsson.interactivemapservice.pojo.testing;

@FunctionalInterface
public interface Foo {
    String method(String string);
}
