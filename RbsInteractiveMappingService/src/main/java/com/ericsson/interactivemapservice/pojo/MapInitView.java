package com.ericsson.interactivemapservice.pojo;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.OptionalDouble;
import java.util.Set;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

// CURRENT SERIALIZED WRAPPER FOR RBS INIT VIEW
public class MapInitView {


    @JsonProperty("rbss")
	@JacksonXmlElementWrapper(useWrapping = false)
	ArrayList<RbsInit> newRbsList;
	
	ArrayList<RbsInitView> rbsListFromDb;
	List<RbsInitView> bufferList;
	Set<String> geohashes;
	
	public MapInitView(final ArrayList<RbsInitView> list) {
		this.rbsListFromDb = list;
		this.newRbsList = new ArrayList<RbsInit>();
		this.bufferList = new ArrayList<RbsInitView>();
		this.geohashes = new HashSet<>();
	}
	
	public void createNewList() {

		boolean firstCheck = true;
		boolean skip = false;
		
		for(int i = 0; i < rbsListFromDb.size() - 1; i++) {
			// assign to local variables for debugging purposes
			RbsInitView currentRbs = rbsListFromDb.get(i);
			RbsInitView nextRbs = rbsListFromDb.get(i+1);
			String eNodeBID1 = currentRbs.getENodeBID();
			String eNodeBID2 = nextRbs.getENodeBID();
			//System.err.println(eNodeBID1);
			
			// remove entries that have a latitude of -90 due to bug in the database
			while((int) nextRbs.getLatitude() == -90) {
				rbsListFromDb.remove(nextRbs);
				nextRbs = rbsListFromDb.get(i+1);
			}
			
			// eNodeBIDs don't match for two elements from rbsListFromDb
			if((eNodeBID1.equals(eNodeBID2) == false)) {
				// to avoid duplication when eNodeBIDs not equal, and are more than 500m apart
				if(skip) {
					skip = false;
					continue;
				}
				// if only one RbsInitView with eNodeBID @ i
				if(firstCheck) {
					HashSet<String> temp = new HashSet<>();
					temp.add(currentRbs.getGeohash());
					
					newRbsList.add(new RbsInit(currentRbs.getNumCells(),
								   currentRbs.getENodeBID(),
								   roundTo4decimalPlaces(currentRbs.getLatitude()),
								   roundTo4decimalPlaces(currentRbs.getLongitude()),
								   currentRbs.getRegion(),
								   currentRbs.getTac(),
								   currentRbs.getRETCapable(),
								   currentRbs.getFreqBand(),
								   temp));
					continue; // to keep firstCheck = true
				}
				
				mergeBufferAndAddToNewRbsList();
				bufferList.clear();
				geohashes.clear();
				
				firstCheck = true;
			}
			// eNodeBIDs for two RBSs match
			else if(currentRbs.equals(nextRbs)) {
				// not first time eNodeBID matches for two consecutive RbsInitViews
				if(!firstCheck) {
					bufferList.add(nextRbs);
				}
				// first time eNodeBID matches for two RbsInitViews, j is incremented to prevent duplicates added to bufferList
				else {
					bufferList.add(currentRbs);
					bufferList.add(nextRbs);
					firstCheck = false;
				}
			}
			// two RBSs have the same eNodeBID but are more than 500m apart
			else {
				// if eNodeBID of currentRbs is already in newRbsList, only add nextRbs
				if(newRbsList.get(newRbsList.size()-1).geteNodeBID().equals(currentRbs.getENodeBID())) {
					HashSet<String> temp = new HashSet<>();
					
					temp.add(nextRbs.getGeohash());
					
					newRbsList.add(new RbsInit(nextRbs.getNumCells(),
							                   nextRbs.getENodeBID(),
							                   roundTo4decimalPlaces(nextRbs.getLatitude()),
							                   roundTo4decimalPlaces(nextRbs.getLongitude()),
											   nextRbs.getRegion(),
											   nextRbs.getTac(),
											   nextRbs.getRETCapable(),
											   nextRbs.getFreqBand(),
							                   temp));
				}
				// if currentRbs's eNodeBID is not in newRbsList, add both
				else {
					HashSet<String> tempCurr = new HashSet<>();
					HashSet<String> tempNext = new HashSet<>();
					
					tempCurr.add(currentRbs.getGeohash());
					tempNext.add(nextRbs.getGeohash());
					
					newRbsList.add(new RbsInit(currentRbs.getNumCells(),
											   currentRbs.getENodeBID(),
							   				   roundTo4decimalPlaces(currentRbs.getLatitude()),
							   				   roundTo4decimalPlaces(currentRbs.getLongitude()),
											   currentRbs.getRegion(),
											   currentRbs.getTac(),
											   currentRbs.getRETCapable(),
											   currentRbs.getFreqBand(),
							   				   tempCurr));
					
					newRbsList.add(new RbsInit(nextRbs.getNumCells(),
											   nextRbs.getENodeBID(),
											   roundTo4decimalPlaces(nextRbs.getLatitude()),
											   roundTo4decimalPlaces(nextRbs.getLongitude()),
											   nextRbs.getRegion(),
											   nextRbs.getTac(),
											   nextRbs.getRETCapable(),
											   nextRbs.getFreqBand(),
											   tempNext));
					skip = true;
				}
			}
		}
	}
	
	public void mergeBufferAndAddToNewRbsList() {
		// make 1 RBS from bufferList and add it to newRbsList
		long numCells = 0;
		
		for(RbsInitView r: bufferList) {
			numCells += r.getNumCells();
		}
		
		// create stream of latitudes from bufferList and get average value as double
		double averageLat = bufferList.stream()
				  .mapToDouble(RbsInitView::getLatitude)
				  .average()
				  .orElse(Double.NaN);
		
		// create stream of longitudes from bufferList and get average value as double
		double averageLon = bufferList.stream()
				  .mapToDouble(RbsInitView::getLongitude)
				  .average()
				  .orElse(Double.NaN);
		
		// round latitude and longitude to 4 decimal places
		averageLat = roundTo4decimalPlaces(averageLat);
		averageLon = roundTo4decimalPlaces(averageLon);
		
		// geohashes set contains all unique goehashes from bufferList
		bufferList.forEach(r -> geohashes.add(r.getGeohash()));
		
		newRbsList.add(new RbsInit(numCells, bufferList.get(0).getENodeBID(), averageLat, averageLon, bufferList.get(0).getRegion(), bufferList.get(0).getTac(), bufferList.get(0).getRETCapable(), bufferList.get(0).getFreqBand(), geohashes));
	}
	
	// utility method to round a double to 4 decimal places
	public double roundTo4decimalPlaces(double l) {
		return Math.round(l * 10000.0) / 10000.0;
	}
	
	@JsonIgnore
	public ArrayList<RbsInit> getNewRbsList() {
		return newRbsList;
	}

	@JsonIgnore
	public ArrayList<RbsInitView> getRbsListFromDb() {
		return rbsListFromDb;
	} 

}
