package com.ericsson.interactivemapservice.pojo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Immutable;

@Deprecated
// deprecated RBS POJO, used to query DB
@Entity
@Immutable
@Table(name = "sectors")
public class RbsEntity {
	
	@Id
	@Column(name = "eNodeBID")
	private String eNodeBID;
	
	@Column(name = "latitude")
	private String latitude;
	
	@Column(name = "longitude")
	private String longitude;
	

	
	public RbsEntity() {}

	public RbsEntity(String eNodeBID, String latitude, String longitude) {
		super();
		this.eNodeBID = eNodeBID;
		this.latitude = latitude;
		this.longitude = longitude;
	}

	public String geteNodeBID() {
		return eNodeBID;
	}

	public String getLatitude() {
		return latitude;
	}

	public String getLongitude() {
		return longitude;
	}
	

}
