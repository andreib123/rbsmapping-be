package com.ericsson.interactivemapservice.repository;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ericsson.interactivemapservice.pojo.CellClickEntity;
import com.ericsson.interactivemapservice.pojo.CellEntity;
import com.ericsson.interactivemapservice.pojo.RbsInitView;
import com.ericsson.interactivemapservice.pojo.RbsLocation;
import com.ericsson.interactivemapservice.pojo.RbsEntityForInitView;
import com.ericsson.interactivemapservice.pojo.RbsForSerialization;

@Component
@Repository
@Transactional(readOnly = true)
public interface SuperDBFinalRepository extends JpaRepository<CellEntity, String> {
	
	// returns a list of cells of an RBS, identified by eNodeBID
	@Query("SELECT c FROM CellEntity c WHERE c.eNodeBID = :eNodeBID and c.cellType = 'Macro'")
	public ArrayList<CellEntity> findCellsByENodeBID(@Param("eNodeBID") String eNodeBID);

	// returns a list of RBSs, limited to 100 and sorted in ascending eNodeBID values for the initial home view
	//@Query("SELECT r.eNodeBID, avg(r.latitude), avg(r.longitude) FROM RbsEntity r WHERE (r.latitude != 0) AND (r.longitude != 0) GROUP BY r.eNodeBID")
	@Query("SELECT new com.ericsson.interactivemapservice.pojo.RbsLocation("
			+ "r.eNodeBID as eNodeBID, AVG(r.latitude) AS latitude, AVG(r.longitude) AS longitude) "
			+ "FROM RbsEntity r WHERE (r.latitude != 0) AND (r.longitude != 0) GROUP BY r.eNodeBID")
	public Page<RbsLocation> findInitRbsView(Pageable top100SortedAsc);
	
	@Query("SELECT new com.ericsson.interactivemapservice.pojo.RbsForSerialization("
			+ "COUNT(r) as numCells, r.latitude, r.longitude, r.eNodeBID as eNodeBIDe) "
			+ "FROM RbsEntityForQuery r WHERE r.cellType = 'Macro'"
			+ "GROUP BY r.eNodeBID ORDER BY r.eNodeBID")
	public ArrayList<RbsForSerialization> findRbsForSerialization();

	// CURRENT INITIAL VIEW REQUEST
	@Query("SELECT new com.ericsson.interactivemapservice.pojo.RbsInitView("
			+ "COUNT(r) as numCells, r.geohash as geohash, r.eNodeBID as eNodeBID, r.region AS region, r.tac AS tac, r.RETCapable AS RETCapable, r.freqBand AS freqBand) "
			+ "FROM RbsEntityForInitView r WHERE r.cellType = 'Macro' AND r.geohash IS NOT NULL "
			+ "GROUP BY r.geohash ORDER BY r.eNodeBID")
	public ArrayList<RbsInitView> findRbsInitView();

	// CURRENT RBS CLICK VIEW REQUEST
	@Query("SELECT c FROM CellClickEntity c WHERE c.cellType = 'Macro' AND c.geohash in :geohashes")
	public ArrayList<CellClickEntity> findCellInfoForRbs(@Param("geohashes") List<String> geohashes);
}
