package com.ericsson.interactivemapservice.repository;

import java.util.ArrayList;
import java.util.List;

import com.ericsson.interactivemapservice.pojo.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;

@Component
public class RepositoryService {
	
	@Autowired
	private SuperDBFinalRepository superDBFinal;
	
	
	public RbsCellStore getCellInfoFromSuperDB(String eNodeBID) {
		return new RbsCellStore(superDBFinal.findCellsByENodeBID(eNodeBID));
	}
	
	public MapRbsStore getRbsInitInfoFromSuperDB() {
		Pageable top100SortedAsc = PageRequest.of(0, 100, Sort.by("eNodeBID").ascending());
		
		Page<RbsLocation> pagedRbsResult = superDBFinal.findInitRbsView(top100SortedAsc);
		return new MapRbsStore(pagedRbsResult.getContent());
		
	}

	public MapForSerialization getRbsInit() {
		return new MapForSerialization(superDBFinal.findRbsForSerialization());
	}
	
	public MapInitView getInitViewInfoFromSuperDb() {
		MapInitView map = new MapInitView(superDBFinal.findRbsInitView());
		map.createNewList();
		return map;
	}

	public ArrayList<CellClickEntity> getRbsInfoFromSuperDb(List<String> geohashes) {
		return superDBFinal.findCellInfoForRbs(geohashes);
	}
}
