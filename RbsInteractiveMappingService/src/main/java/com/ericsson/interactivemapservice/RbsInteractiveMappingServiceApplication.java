package com.ericsson.interactivemapservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RbsInteractiveMappingServiceApplication {
	public static void main(final String[] args) {
		
		SpringApplication.run(RbsInteractiveMappingServiceApplication.class, args);
		
	}
}



