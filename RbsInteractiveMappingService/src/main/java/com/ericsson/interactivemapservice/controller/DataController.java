package com.ericsson.interactivemapservice.controller;

import java.util.ArrayList;
import java.util.List;

import com.ericsson.interactivemapservice.pojo.CellClickEntity;
import com.ericsson.interactivemapservice.pojo.MapInitView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ericsson.interactivemapservice.repository.RepositoryService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;

@RestController
@RequestMapping(

				produces = { MediaType.APPLICATION_JSON_VALUE,
							 MediaType.APPLICATION_XML_VALUE ,
							 MediaType.TEXT_PLAIN_VALUE},
				
				consumes = { MediaType.APPLICATION_JSON_VALUE,
							 MediaType.APPLICATION_XML_VALUE,
							 MediaType.TEXT_PLAIN_VALUE })

public class DataController {
	private final static Logger logger = LoggerFactory.getLogger(DataController.class);

	@Autowired
	private DataService dataService;
	
	@Autowired
	private RepositoryService repositoryService;

	@Deprecated
	@RequestMapping(value = "/resources/files/rbs/xmldata", method = RequestMethod.GET)
	public FileSystemResource getXmlDoc() {
		logger.info("/home/resources/files/rbs/xmldata endpoint is called");
		return dataService.getDataFromFile("src/main/resources/files/rbs_map_data.xml");
	}

	@Deprecated
	@RequestMapping(value = "/objects/onerbstest", method = RequestMethod.GET)
	public String getOneRbsObj() throws JsonProcessingException {
		logger.info("localhost:8080/rbs/map/objects/onerbstest endpoint is called");
		XmlMapper xmlMapper = new XmlMapper();
		return xmlMapper.writeValueAsString(dataService.getOneRbs());
	}

	@Deprecated
	@RequestMapping(value = "/home", method = RequestMethod.GET)
	public String getHomeViewInit() throws JsonProcessingException {
		logger.info("localhost:8080/rbs/map/home endpoint is called");
		XmlMapper xmlMapper = new XmlMapper();
		return xmlMapper.writeValueAsString(repositoryService.getRbsInitInfoFromSuperDB());
	}

	@Deprecated
	@RequestMapping(value = "/objects/rbscells", method = RequestMethod.GET)
	public String getCellInfoOneRbs(
		@RequestParam("eNodeBID") String eNodeBID) throws JsonProcessingException {
			logger.info("localhost:8080/rbs/map/objects/rbscells?eNodeBID=X endpoint is called");
			XmlMapper xmlMapper = new XmlMapper();
			return xmlMapper.writeValueAsString(repositoryService.getCellInfoFromSuperDB(eNodeBID));
	}

	@Deprecated
	@RequestMapping(value = "/objects/view/init/test", method = RequestMethod.GET)
	public String getMapInitViewTest() throws JsonProcessingException {
		logger.info("localhost:8080/rbs/map/objects/view/init/test endpoint is called");
		XmlMapper xmlMapper = new XmlMapper();
		return xmlMapper.writeValueAsString(repositoryService.getRbsInit());
	}

	// current init view
	@RequestMapping(value = "/objects/view/init", method = RequestMethod.GET,
					consumes = MediaType.APPLICATION_JSON_VALUE,
					produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public MapInitView getMapInitView() throws JsonProcessingException {
		logger.info("localhost:8080/rbs/map/objects/view/init endpoint is called");
		return repositoryService.getInitViewInfoFromSuperDb();
	}
	
	// current rbs click view
	@RequestMapping(value = "/objects/view/rbs", method = RequestMethod.GET,
					consumes = MediaType.APPLICATION_JSON_VALUE,
					produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public ArrayList<CellClickEntity> getRbsSpecificView(
		@RequestParam("geohash") List<String> geohashes) throws JsonProcessingException {
			logger.info("localhost:8080/rbs/map/objects/view/rbs?geohash={} endpoint is called", geohashes);
			return repositoryService.getRbsInfoFromSuperDb(geohashes);
	}
	
}
