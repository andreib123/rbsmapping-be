package com.ericsson.interactivemapservice.controller;

import java.util.ArrayList;

import org.springframework.core.io.FileSystemResource;
import org.springframework.stereotype.Component;

import com.ericsson.interactivemapservice.pojo.Cell;
import com.ericsson.interactivemapservice.pojo.Location;
import com.ericsson.interactivemapservice.pojo.Map;
import com.ericsson.interactivemapservice.pojo.Rbs;

@Deprecated
@Component
public class DataService {

	public FileSystemResource getDataFromFile(String path) {
		return new FileSystemResource(path);
	}

	public Map getOneRbs() {
		
		ArrayList<String> technologies2_3_G = new ArrayList<String>();

		ArrayList<Cell> cell = new ArrayList<Cell>();
		ArrayList<Rbs> rbss = new ArrayList<Rbs>();
		
		
		technologies2_3_G.add("GSM");
		technologies2_3_G.add("UMTS");
		//cell.add(new Cell("Vodafone", 01, 280, technologies2_3_G));
		//cell.add(new Cell("Vodafone", 02, 90, technologies2_3_G));
		rbss.add(new Rbs("Summerhill", 01, new Location(53.404861, -7.998116), cell));
		
		return new Map(rbss);
	}
	
}
